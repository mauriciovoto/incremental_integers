require 'spec_helper'

describe IncrementalIntegers::Signup::Service do
  describe '.run' do
    subject { described_class.new(email, password).run }

    context 'given valid email and password' do
      let(:email) { 'luke_s@starwars.com' }
      let(:password) { 'l3i4ismysist3r' }
      before { allow(Digest::MD5).to receive(:hexdigest).and_return('123') }

      it { is_expected.to eq('123') }
    end

    context 'given an existing api key' do
      let(:email) { 'luke_s@starwars.com' }
      let(:password) { 'l3i4ismysist3r' }
      let!(:api_key) { described_class.new(email, password).run }

      it { is_expected.to eq(api_key) }
    end

    context 'given an empty email' do
      let(:email) { "" }
      let(:password) { 'l3i4ismysist3r' }

      it { is_expected.to be_nil }
    end

    context 'given an empty password' do
      let(:email) { 'luke_s@starwars.com' }
      let(:password) { "" }

      it { is_expected.to be_nil }
    end

    context 'given an invalid email' do
      let(:email) { 'darthvader.com' }
      let(:password) { 'd34thstar' }

      it { is_expected.to be_nil }
    end

    context 'given an invalid password' do
      let(:email) { 'c3po@star_wars.com' }
      let(:password) { 'r2d2' }

      it { is_expected.to be_nil }
    end
  end
end
