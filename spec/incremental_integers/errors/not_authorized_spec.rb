require 'spec_helper'

describe IncrementalIntegers::Errors::NotAuthorized do
  describe '.to_rack' do
    it 'returns an error-like rack response' do
      expect(described_class.to_rack).to eq([
        401,
        {'Content-Type' => 'application/json'},
        [{message: 'Not Authorized'}.to_json]
      ])
    end
  end
end
