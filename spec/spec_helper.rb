require 'simplecov'

SimpleCov.start do
  refuse_coverage_drop
  add_filter '/spec/'
end

require 'rspec'
require 'fakeredis'
require 'fakeredis/rspec'
require 'pry'
require 'rack/test'
require './lib/incremental_integers'

include Rack::Test::Methods
ENV['RACK_ENV'] = 'test'

RSpec.configure do |config|
  config.before(:each) do
    allow(Redis).to receive(:new).and_return(Redis.new)
    IncrementalIntegers::Data.persist('number')
  end
end
