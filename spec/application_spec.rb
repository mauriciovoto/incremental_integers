require 'spec_helper'

describe Application do
  let(:app) { Application }
  let(:valid_key) { '112233' }
  before { IncrementalIntegers::Data.save(valid_key, 1) }

  describe 'GET next' do
    it 'increments the number' do
      header 'Authorization', '112233'
      get 'api/v1/next'
      expect(last_response.body).to eq({number: "1"}.to_json)
    end

    context 'when calling multiple times' do
      it 'increments the number' do
        header 'Authorization', '112233'
        2.times { get 'api/v1/next' }
        expect(last_response.body).to eq({number: "2"}.to_json)
      end
    end
  end

  describe 'GET current' do
    context 'given unexistent api key on header' do
      it 'checks for auth' do
        header 'Authorization', '128qwe7813uiew871ui3'
        get 'api/v1/current'

        expect(last_response.status).to eq(401)
      end
    end

    it 'fetches the actual number' do
      header 'Authorization', '112233'
      get 'api/v1/current'
      expect(last_response.body).to eq({number: "0"}.to_json)
    end

    context 'given several iterations' do
      it 'fetches the actual number' do
        header 'Authorization', '112233'
        3.times { get 'api/v1/next' }
        get 'api/v1/current'
        expect(last_response.body).to eq({number: "3"}.to_json)
      end
    end
  end

  describe 'PUT current' do
    it 'returns the updated value of the current number' do
      header 'Authorization', '112233'
      put 'api/v1/current', current: 4
      expect(last_response.body).to eq({number: "4"}.to_json)
    end

    it 'sets the current value' do
      header 'Authorization', '112233'
      put 'api/v1/current', current: 7
      get 'api/v1/current'
      expect(last_response.body).to eq({number: "7"}.to_json)
    end
  end

  describe 'POST signup' do
    context 'given valid email and password' do
      let(:email) { 'luke_s@starwars.com' }
      let(:password) { 'l3i4ismysist3r' }
      before { allow(Digest::MD5).to receive(:hexdigest).and_return('123') }

      it 'returns an api key' do
        post '/api/v1/signup', email: email, password: password
        expect(last_response.body).to eq({api_key: '123'}.to_json)
      end
    end
  end
end
