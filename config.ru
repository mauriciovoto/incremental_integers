require 'rack/cors'
use Rack::Cors do
  allow do
    origins '*'
    resource '/api/v1/*', headers: :any, methods: [:get, :put, :post]
  end
end

ENV['RACK_ENV'] = 'development' if ENV['RACK_ENV'].nil?
require './lib/incremental_integers'
IncrementalIntegers::Data.persist(:number)
run Application
