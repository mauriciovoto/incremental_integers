Thinkific Challenge
===================

This is a simple REST API that is built using pure rack.
It consists on a service to handle a sequential integer number.

## Installation

This application depends on ruby [2.4.1 version](https://www.ruby-lang.org/en/documentation/installation/)(
it is recommended to use rbenv or rvm to manage ruby versions).
Another dependency is the in-memory data structure store: [redis](https://redis.io/topics/quickstart).
After installing ruby, just run the setup script to install dependencies and
copy the necessary files:

```
./script/setup.sh
```

**Before try to run the application, please make sure that redis path on the
Procfile is correct, according to your installation.**

## Running the Application

The application uses [foreman gem](https://github.com/ddollar/foreman) to
simplify the startup. To get it running, just have to run the following command:

```
bundle exec foreman start -f Procfile.dev
```

### Running the test suite

When running the test suite, a HTML coverage report is generated at `./coverage`

```
bundle exec rspec .
```

### Executing code metrics analyzer

It generates a HTML metrics report on the specified output path.

```
sandi_meter -g -o <output_path>
```

## API Endpoints

### GET /api/v1/next

* **Header:**

```
Content-Type: application/json
Authorization: <API_KEY>
```

* **Curl Request Example:**

Assuming that an api key was previously generated, ie: `1f702601ba047bdd8eecb657da3cc074`

```
curl -i http://localhost:3000/api/v1/next -H "Authorization: 1f702601ba047bdd8eecb657da3cc074"
```

* **Response:**

```
HTTP/1.1 200 OK
Content-Type: application/json

{"number":"17"}%
```


* **Errors:**

```
HTTP/1.1 401 Unauthorized
Content-Type: application/json

{"message": "Not Authorized"}
```

### GET /api/v1/current

* **Header:**

```
Content-Type: application/json
Authorization: <API_KEY>
```

* **Params:**

```
current: <current_value>
```

* **Curl Request Example:**

Assuming that an api key was previously generated, ie: `1f702601ba047bdd8eecb657da3cc074`

```
http://localhost:3000/api/v1/current -H "Authorization: 1f702601ba047bdd8eecb657da3cc074"
```

* **Response:**

```
HTTP/1.1 200 OK
Content-Type: application/json

{"number":"17"}%
```

* **Errors:**

```
HTTP/1.1 401 Unauthorized
Content-Type: application/json

{"message": "Not Authorized"}
```

### PUT /api/v1/current

* **Header:**

```
Content-Type: application/json
Authorization: <API_KEY>
```

* **Params:**

```
current: <current_value>
```

* **Curl Request Example:**

Assuming that an api key was previously generated, ie: `1f702601ba047bdd8eecb657da3cc074`

```
curl -i -X "PUT" http://localhost:3000/api/v1/current --data "current=10" -H "Authorization: 1f702601ba047bdd8eecb657da3cc074"
```

* **Response:**

```
HTTP/1.1 200 OK
Content-Type: application/json

{"number":"10"}%
```

* **Errors:**

```
HTTP/1.1 401 Unauthorized
Content-Type: application/json

{"message": "Not Authorized"}
```

### POST /api/v1/signup

* **Header:**

```
Content-Type: application/json
```

* **Params:**

```
email: <email>
password: <password>
```

* **Response:**

```
HTTP/1.1 201 OK
Content-Type: application/json

{"api_key":"1f702601ba047bdd8eecb657da3cc074"}%
```

* **Curl Request Example:**

```
curl -i -X POST -d 'email=want_to_work@thinkific.com&password=P4sSw0rD' http://localhost:3000/api/v1/signup -H "Content-Type: application/json"
```

## Assumptions / Decisions

This section ilustrates some of assumptions and decisions took for the
development process. The main focus was to keep it simple, extensible and legible.

### Rack + Redis

The main reason to choose rack and redis was simplicity + performance. Since
pure rack has less dependencies than Sinatra, Grape and Rails, it responds
[faster](https://blog.altoros.com/performance-comparison-of-ruby-frameworks-app-servers-template-engines-and-orms-q4-2016.html)
and it's also pretty simple to use(added the elegant rack-api gem to help with
some sugar dsl). Same points for redis, the "memory stored database". To have a
permanent value for the sequential number, it was used the `.persist` on the number
key, to remove the ttl.

### Strategy

To start the development, it was decided to start by the number handling
enpoints, so a basic redis setup with the respective calls at the endpoints was
the first move.
A simple signup + authentication was the next step. The decision to avoid using
devise was took once again, thinking about simplicity(but I would definitely
reconsider it if this API would evolve with more significant user data). The
`NotAuthorized` error class was added to handle requests without or with wrong
api key.
The next decision were related to refactors: to make the `Application` class a
router and move the logic to specific and single responsibilites controller
classes; move redis logic to a data wrapper class to get isolated, so it would
be easier to replace redis to a database, such as postgres or mongo for example;
and move the unexistent `Auth` class to a `Signup` service and key generator
classes.

### Incremental Feature

The assumption was that only one sequence number would be handled by many
different api_keys. If the desired feature would have to be one sequential
number per each api_key, than the `number key` would have to be moved to the
signup hashes stored on redis.

## What's next / TODO List

- Handle more error for enpoint situations for params, ie: missing params
- Add some security precautions, ie: adding rack-attack gem
- Move API docs to something like swagger api for example
