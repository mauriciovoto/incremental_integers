require 'active_model'

module IncrementalIntegers
  module Signup
    class Service
      include ActiveModel::Model

      EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
      validates :email, :password, presence: true
      validates :email, format: { with: EMAIL_REGEX }
      validates :password, length: { minimum: 6 }

      def initialize(email, password)
        @email, @password = email, password
        @key_generator = KeyGenerator.new(email, password)
        @result_hash = Data.find_hash(email, :email, :password, :api_key)
      end

      def run
        return nil unless valid?
        return key_generator.new_key if result_hash.values.all?(&:nil?)
        fetch_key if password_correct?
      end

      private
      attr_reader :email, :password, :validator, :key_generator, :result_hash

      def password_correct?
        result_hash[:password] == password
      end

      def fetch_key
        result_hash[:api_key]
      end
    end
  end
end
