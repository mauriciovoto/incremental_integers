require 'digest'

module IncrementalIntegers
  module Signup
    class KeyGenerator
      def initialize(email, password)
        @email, @password = email, password
      end

      def new_key
        api_key = secure_key
        save(api_key)

        api_key
      end

      private
      attr_reader :email, :password

      def save(api_key)
        Data.save_hash(email, {email: email, password: password, api_key: api_key})
        Data.save(api_key, 1)
      end

      def secure_key
        random = Random.new.rand(100..9000)
        Digest::MD5.hexdigest("#{email}#{password}#{random}")
      end
    end
  end
end
