module IncrementalIntegers
  module Errors
    class NotAuthorized < StandardError
      def self.to_rack
        [
          401,
          {'Content-Type' => 'application/json'},
          [{message: 'Not Authorized'}.to_json]
        ]
      end
    end
  end
end
