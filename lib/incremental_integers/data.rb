require 'yaml'

module IncrementalIntegers
  class Data
    class << self
      def increment(attribute_name)
        session.incr(attribute_name)
      end

      def find(attribute_name)
        session.get(attribute_name) || 0
      end

      def save(attribute_name, value)
        session.set(attribute_name, value)
      end

      def save_hash(key, hash)
        session.mapped_hmset(key, hash)
      end

      def find_hash(key, *fields)
        session.mapped_hmget(key, *fields)
      end

      def persist(attribute_name)
        session.persist(attribute_name)
      end

      private
      attr_reader :session

      def session
        if File.exist?('./config/redis.yml')
          config = YAML::load_file(File.join('./config', 'redis.yml'))
          @session ||= Redis.new(config[ENV['RACK_ENV']])
        else
          @session ||= Redis.new(driver: 'ruby', url: ENV['REDIS_URL'])
        end
      end
    end
  end
end
