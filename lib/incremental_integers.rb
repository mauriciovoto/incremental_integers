require 'bundler'
Bundler.require(:default)

module IncrementalIntegers
end

# ORM Classes
require './lib/incremental_integers/data'
# Exception Classes
require './lib/incremental_integers/errors/not_authorized'
# Signup Classes
require './lib/incremental_integers/signup/key_generator'
require './lib/incremental_integers/signup/service'
# Controllers
require './lib/controllers/base_controller'
require './lib/controllers/numbers'
require './lib/controllers/signup'
# Router Class
require './lib/application'
