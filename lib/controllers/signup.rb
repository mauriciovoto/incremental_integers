class Signup < BaseController
  def create
    email, password = params[:email].to_s, params[:password].to_s
    key = IncrementalIntegers::Signup::Service.new(email, password).run

    status 201
    { api_key: key }
  end
end
