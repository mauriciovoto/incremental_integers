class Numbers < BaseController
  def index
    authorize
    number = IncrementalIntegers::Data.increment('number')

    { number: number.to_s }
  end

  def show
    authorize
    number = IncrementalIntegers::Data.find('number')

    { number: number.to_s }
  end

  def update
    authorize
    number = params[:current].to_i
    IncrementalIntegers::Data.save('number', number)

    { number: number.to_s }
  end
end
