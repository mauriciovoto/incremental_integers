require 'rack/api'

class BaseController < Rack::API::Controller
  protected
  def authorize
    key = IncrementalIntegers::Data.find(env['HTTP_AUTHORIZATION'])
    error(IncrementalIntegers::Errors::NotAuthorized) if key.nil? || key != '1'
  end
end
