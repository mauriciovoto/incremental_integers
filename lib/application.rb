require './lib/incremental_integers'

class Application < Rack::API
  prefix 'api'

  version :v1 do
    rescue_from IncrementalIntegers::Errors::NotAuthorized do |error|
      [403, {'Content-Type' => 'application/json'}, ['Not Authorized']]
    end

    get '/next', to: 'numbers#index'
    get '/current', to: 'numbers#show'
    put '/current', to: 'numbers#update'

    post '/signup', to: 'signup#create'
  end
end
