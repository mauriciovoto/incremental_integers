#!/bin/bash

if [ -f "./config/redis.yml" ]
then
  echo "Redis config file already exists"
else
  cp ./config/redis.yml.example ./config/redis.yml
fi

if [ -f "./Procfile.dev" ]
then
  echo "Procfile.dev file already exists"
else
  cp ./Procfile.dev.example ./Procfile.dev
fi

if [ -f "./.rspec" ]
then
  echo "Rspec config file already exists"
else
  cp ./.rspec.example ./.rspec
fi

gem install bundler --no-rdoc --no-ri
bundle install
